import React, { FC, useState, useEffect, useMemo, useCallback } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

type IPaginatedUsers = { [pageNumber: string]: IUserProps[] };

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [paginatedUsers, setPaginatedUsers] = useState<IPaginatedUsers>({});
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(12);
  const [totalPages, setTotalPages] = useState<number>();

  const fetchData = useCallback(async () => {
    try {
      if (paginatedUsers[page]) return;
      setLoading(true);
      const result = await backendClient.getAllUsers({ page, pageSize });
      setPaginatedUsers({ ...paginatedUsers, [page]: result.data });
      setTotalPages(result.totalPages);
    } finally {
      setLoading(false);
    }
  }, [page, pageSize, paginatedUsers]);

  useEffect(() => {
    fetchData();
  }, [page, pageSize, fetchData]);

  const users = useMemo(() => {
    return paginatedUsers[page] || [];
  }, [paginatedUsers, page]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <button onClick={() => setPage(page - 1)} disabled={page === 1}>
                Previous
              </button>
              <button
                onClick={() => setPage(page + 1)}
                disabled={!!totalPages && page >= totalPages}
              >
                Next
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
