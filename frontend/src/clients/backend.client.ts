import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

type GetAllUsersQueryParams = {
  page: number;
  pageSize: number;
};

type GetAllUsersResponse = {
  data: IUserProps[];
  totalPages: number;
};

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers({
    page,
    pageSize,
  }: GetAllUsersQueryParams): Promise<GetAllUsersResponse> {
    const queryParams = new URLSearchParams({
      page: page.toString(),
      pageSize: pageSize.toString(),
    });
    const { data } = await axios.get(
      `${this.baseUrl}/people?${queryParams.toString()}`,
      {}
    );
    return data;
  }
}
