import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParam,
} from "routing-controllers";
import { PeopleProcessing } from "../services/people_processing.service";

const peopleProcessing = new PeopleProcessing();

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get("/")
  getAllPeople(
    @QueryParam("page") page: string,
    @QueryParam("pageSize") pageSize: string
  ) {
    const people = peopleProcessing.getAll(+page, +pageSize);

    if (!people) {
      throw new NotFoundError("No people found");
    }

    const peoplesTotal = peopleProcessing.count();
    const totalPages = Math.ceil(peoplesTotal / +pageSize);

    return {
      data: people,
      currentPage: page,
      totalPages,
    };
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }
}
