import people_data from "../data/people_data.json";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(page: number, pageSize: number) {
    const skip = pageSize * (page - 1);
    return people_data.slice(skip, skip + pageSize);
  }

  count() {
    return people_data.length;
  }
}
